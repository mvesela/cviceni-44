// npm run new-cviceni -- --slug cviceni-test-1 --name "Cvičení Test 1"
// notice the double dash "--", see: https://docs.npmjs.com/cli/run-script

// eslint-disable-next-line import/no-extraneous-dependencies
const slugify = require('slugify');
const fs = require('fs');

const pkg = {};
pkg.path = './package.json';
pkg.file = require('../package.json');

// config
const config = {
  path: {
    folder: 'src/scaffolding-templates/cviceni/',
    data: {
      templateFolder: 'src/scaffolding-templates/cviceni/data/',
      srcFolder: 'src/data/',
    },
    views: {
      folder: 'src/scaffolding-templates/cviceni/views/',
      cviceni: {
        folder: 'src/scaffolding-templates/cviceni/views/cviceni/',
      },
    },
    img: {
      folder: 'src/img/',
    },
    audio: {
      folder: 'src/audio/',
    },
    text: {
      folder: 'src/text/',
    },
  },
  placeholders: {
    json: '{REPLACE_NEW_ID_CVICENI_JSON_SOUBOR}',
  },
};


function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

// create JSON data file name from --slug to the camelCase:
// 'name-of-cviceni' –> 'cviceniNameOfCviceni'
function createDataFileName(str) {
  const stringArray = str.split('-');

  stringArray.forEach((part, index) => {
    stringArray[index] = upperCaseFirst(part);
  });

  return `cviceni${stringArray.join('')}`;
}


// command line arguments order
const slugIndex = process.argv.indexOf('--slug');
const nameIndex = process.argv.indexOf('--name');


let slug; let name;
// command line arguments values
if (nameIndex > -1) {
  name = process.argv[nameIndex + 1];
}

if (slugIndex > -1) {
  slug = process.argv[slugIndex + 1];
} else {
  slug = slugify(name, {
    remove: /[!?#=*+~.()'":@]/g,
  }).toLowerCase();
}

// console.log('hello new cviceni');
// console.log('process.argv: ' + process.argv);
// console.log('option slug: ' + slug);

// data file name
const dataFileName = createDataFileName(slug);
// pug folder name
const pugFolderName = `src/views/cviceni/${slug}/`;


// create new PUG file within folder
fs.mkdir(pugFolderName, (error) => {
  if (error && (error.code !== 'EEXIST')) throw error;

  // scaffold views
  fs.readFile(`${config.path.views.cviceni.folder}index.pug`, 'utf8', (error, data) => {
    if (error) throw error;

    // replace index.pug variable name
    const result = data.replace(new RegExp(config.placeholders.json, 'g'), dataFileName);

    // copy the file to the cviceni folder
    fs.writeFile(`${pugFolderName}index.pug`, result, 'utf8', (error) => {
      if (error) throw error;
      console.log(`Pug file "src/views/cviceni/${slug}/index.pug" has been created.`);
    });
  });

});

// create new JSON data file
fs.readFile(`${config.path.data.templateFolder}cviceniNewData.json`, 'utf8', (error, data) => {
  if (error) throw error;

  const templateData = JSON.parse(data);

  templateData.cviceni.id = pkg.file.exercise.number + 1;
  pkg.file.exercise.number = templateData.cviceni.id;
  templateData.cviceni.slug = slug;
  templateData.cviceni.nazev = name;
  templateData.cviceni.pdf.doporucenyPostup = `${slug}.pdf`;

  // copy the file to the cviceni folder
  fs.writeFile(`${config.path.data.srcFolder + dataFileName}.json`, JSON.stringify(templateData, null, 2), 'utf8', (error) => {
    if (error) throw error;
    console.log(`Data file "src/data/${dataFileName}.json" has been created.`);
  });

  // update the number of exercises in package.json for next exercise
  fs.writeFile(pkg.path, JSON.stringify(pkg.file, null, 2), 'utf8', (error) => {
    if (error) throw error;
  });

});

// create new img folder
fs.mkdir(config.path.img.folder + slug, (error) => {
  if (error && (error.code !== 'EEXIST')) throw error;
  console.log(`Image folder "src/img/${slug}/" has been created.`);
});
// create new audio folder
fs.mkdir(config.path.audio.folder + slug, (error) => {
  if (error && (error.code !== 'EEXIST')) throw error;
  console.log(`Audio folder "src/audio/${slug}/" has been created.`);
});
// create new text folder
fs.mkdir(config.path.text.folder + slug, (error) => {
  if (error && (error.code !== 'EEXIST')) throw error;
  console.log(`Text folder "src/text/${slug}/" has been created.`);
});

// TODO: log after callback
// console.log('==================');
// console.log(`SUCCESS! "${name}" has been created.`);
// console.log('==================');
